<?php


/**
 * Get an array of information about modules that support an API.
 *
 * This will ask each module if they support the given API, and if they do
 * it will return an array of information about the modules that do.
 *
 * This function invokes hook_ctools_api. This invocation is statically
 * cached, so feel free to call it as often per page run as you like, it
 * will cost very little.
 *
 * This function can be used as an alternative to module_implements and can
 * thus be used to find a precise list of modules that not only support
 * a given hook (aka 'api') but also restrict to only modules that use
 * the given version. This will allow multiple modules moving at different
 * paces to still be able to work together and, in the event of a mismatch,
 * either fall back to older behaviors or simply cease loading, which is
 * still better than a crash.
 *
 * @param $owner
 *   The name of the module that controls the API.
 * @param $api
 *   The name of the api. The api name forms the file name:
 *   $module.$api.inc
 * @param $minimum_version
 *   The lowest version API that is compatible with this one. If a module
 *   reports its API as older than this, its files will not be loaded. This
 *   should never change during operation.
 * @param $current_version
 *   The current version of the api. If a module reports its minimum API as
 *   higher than this, its files will not be loaded. This should never change
 *   during operation.
 *
 * @return
 *   An array of API information, keyed by module. Each module's information will
 *   contain:
 *   - 'version': The version of the API required by the module. The module
 *     should use the lowest number it can support so that the widest range
 *     of supported versions can be used.
 *   - 'path': If not provided, this will be the module's path. This is
 *     where the module will store any subsidiary files. This differs from
 *     plugin paths which are figured separately.
 *
 *   APIs can request any other information to be placed here that they might
 *   need. This should be in the documentation for that particular API.
 */
function ctools_plugin_api_info($owner, $api, $minimum_version, $current_version) {
  $cache = &drupal_static(__FUNCTION__, array());
  if (!isset($cache[$owner][$api])) {
    $cache[$owner][$api] = array();

    $hook = ctools_plugin_api_get_hook($owner, $api);

    foreach (module_implements($hook) as $module) {
      $function = $module . '_' . $hook;
      $info = $function($owner, $api);
      // This is added to make hook_views_api() compatible with this, since
      // views used a different version key.
      if (isset($info['version'])) {
        $version = $info['version'];
      }
      else if (isset($info['api'])) {
        $version = $info['api'];
      }

      if (!isset($version)) {
        continue;
      }

      // Only process if version is between minimum and current, inclusive.
      if (version_compare($version, $minimum_version, '>=') && version_compare($version, $current_version, '<=')) {
        if (!isset($info['path'])) {
          $info['path'] = drupal_get_path('module', $module);
        }
        $cache[$owner][$api][$module] = $info;
      }
    }

    // And allow themes to implement these as well.
    $themes = _ctools_list_themes();
    foreach ($themes as $name => $theme) {
      if (!empty($theme->info['api'][$owner][$api])) {
        $info = $theme->info['api'][$owner][$api];
        if (!isset($info['version'])) {
          continue;
        }

        // Only process if version is between minimum and current, inclusive.
      if (version_compare($info['version'], $minimum_version, '>=') && version_compare($info['version'], $current_version, '<=')) {
          if (!isset($info['path'])) {
            $info['path'] = '';
          }
          // Because themes can't easily specify full path, we add it here
          // even though we do not for modules:
          $info['path'] = drupal_get_path('theme', $name) . '/' . $info['path'];
          $cache[$owner][$api][$name] = $info;
        }
      }
    }
  }

  return $cache[$owner][$api];
}

/**
 * Load a group of API files.
 *
 * This will ask each module if they support the given API, and if they do
 * it will load the specified file name. The API and the file name
 * coincide by design.
 *
 * @param $owner
 *   The name of the module that controls the API.
 * @param $api
 *   The name of the api. The api name forms the file name:
 *   $module.$api.inc, though this can be overridden by the module's response.
 * @param $minimum_version
 *   The lowest version API that is compatible with this one. If a module
 *   reports its API as older than this, its files will not be loaded. This
 *   should never change during operation.
 * @param $current_version
 *   The current version of the api. If a module reports its minimum API as
 *   higher than this, its files will not be loaded. This should never change
 *   during operation.
 *
 * @return
 *   The API information, in case you need it.
 */
function ctools_plugin_api_include($owner, $api, $minimum_version, $current_version) {
  static $already_done = array();

  $info = ctools_plugin_api_info($owner, $api, $minimum_version, $current_version);
  foreach ($info as $module => $plugin_info) {
    if (!isset($already_done[$owner][$api][$module])) {
      if (isset($plugin_info["$api file"])) {
        $file = isset($plugin_info["$api file"]);
      }
      else if (isset($plugin_info['file'])) {
        $file = isset($plugin_info['file']);
      }
      else {
        $file = "$module.$api.inc";
      }

      if (file_exists(DRUPAL_ROOT . "/$plugin_info[path]/$file")) {
        require_once DRUPAL_ROOT . "/$plugin_info[path]/$file";
      }
      else if (file_exists(DRUPAL_ROOT . "/$file")) {
        require_once DRUPAL_ROOT . "/$plugin_info[path]/$file";
      }
      $already_done[$owner][$api][$module] = TRUE;
    }
  }

  return $info;
}

/**
 * Find out what hook to use to determine if modules support an API.
 *
 * By default, most APIs will use hook_ctools_plugin_api, but some modules
 * want sole ownership. This technique lets modules define what hook
 * to use.
 */
function ctools_plugin_api_get_hook($owner, $api) {
  // Allow modules to use their own hook for this. The only easy way to do
  // this right now is with a magically named function.
  if (function_exists($function = $owner . '_' . $api . '_hook_name')) {
    $hook = $function();
  }
  else if (function_exists($function = $owner . '_ctools_plugin_api_hook_name')) {
    $hook = $function();
  }

  // Do this last so that if the $function above failed to return, we have a
  // sane default.
  if (empty($hook)) {
    $hook = 'ctools_plugin_api';
  }

  return $hook;
}
