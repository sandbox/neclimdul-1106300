<?php
/**
 * @file
 *
 * Contains routines to organize and load plugins. It allows a special
 * variation of the hook system so that plugins can be kept in separate
 * .inc files, and can be either loaded all at once or loaded only when
 * necessary.
 */

/**
 * Get a function from a plugin, if it exists. If the plugin is not already
 * loaded, try plugins_load_function() instead.
 *
 * @param $plugin_definition
 *   The loaded plugin type.
 * @param $function_name
 *   The identifier of the function. For example, 'settings form'.
 *
 * @return
 *   The actual name of the function to call, or NULL if the function
 *   does not exist.
 */
function plugins_get_function($plugin_definition, $function_name) {

  // Validate that it makes sense to continue looking for a function.
  if (!isset($plugin_definition[$function_name])) {
    return;
  }

  // If needed, include an include containing the plugin function. This can be
  // aside from putting the function in the same file as your definition be if
  // you have a function you share between plugins or if the function is
  // actually implemented by another module.
  if (is_array($plugin_definition[$function_name]) && isset($plugin_definition[$function_name]['function'])) {
    $function = $plugin_definition[$function_name]['function'];
    if (isset($plugin_definition[$function_name]['file'])) {
      $file = $plugin_definition[$function_name]['file'];
      if (isset($plugin_definition[$function_name]['path'])) {
        $file = $plugin_definition[$function_name]['path'] . '/' . $file;
      }
      require_once DRUPAL_ROOT . '/' . $file;
    }
  }
  else {
    $function = $plugin_definition[$function_name];
  }

  if (function_exists($function)) {
    return $function;
  }
}

/**
 * Load a plugin and get a function name from it, returning success only
 * if the function exists.
 *
 * @param $module
 *   The module that owns the plugin type.
 * @param $type
 *   The type of plugin.
 * @param $id
 *   The id of the specific plugin to load.
 * @param $function_name
 *   The identifier of the function. For example, 'settings form'.
 *
 * @return
 *   The actual name of the function to call, or NULL if the function
 *   does not exist.
 */
function plugins_load_function($module, $type, $id, $function_name) {
  $plugin = plugins_get_plugin($module, $type, $id);
  return plugins_get_function($plugin, $function_name);
}

/**
 * Get a class from a plugin, if it exists. If the plugin is not already
 * loaded, try plugins_load_class() instead.
 *
 * @param $plugin_definition
 *   The loaded plugin type.
 * @param $plugin_name
 *   The identifier of the class. For example, 'handler'.
 *
 * @return
 *   The actual name of the class to call, or NULL if the class does not exist.
 */
function plugins_get_class($plugin_definition, $plugin_name) {

  // Validate that it makes sense to continue looking for a class.
  if (!isset($plugin_definition[$plugin_name])) {
    return;
  }

  // Check a couple ways the class name could be specified.
  if (is_string($plugin_definition[$plugin_name])) {
    // Plugin uses the string form shorthand.
    $class_name = $plugin_definition[$plugin_name];
  }
  else if (isset($plugin_definition[$plugin_name]['class'])) {
    // Plugin uses the verbose array form.
    $class_name = $plugin_definition[$plugin_name]['class'];
  }
  else {
    // @todo consider adding a watchdog for this failure.
    $class_name = FALSE;
  }

  return ($class_name && class_exists($class_name)) ? $class_name : NULL;
}

/**
 * Load a plugin and get a class name from it, returning success only if the
 * class exists.
 *
 * @param $module
 *   The module that owns the plugin type.
 * @param $type
 *   The type of plugin.
 * @param $id
 *   The id of the specific plugin to load.
 * @param $plugin_name
 *   The identifier of the class. For example, 'handler'.
 *
 * @return
 *   The actual name of the class to call, or NULL if the class does not exist.
 */
function plugins_load_class($module, $type, $id, $plugin_name) {
  $plugin = plugins_get_plugin($module, $type, $id);
  return plugins_get_class($plugin, $plugin_name);
}

/**
 * Fetch a specific plugin definition.
 *
 * @param $module
 *   The name of the module that implements this plugin type.
 * @param $type
 *   The plugin type identifier.
 * @param $id
 *   The identifier of the plugin.
 *
 * @return
 *   The plugin definition array of the plugin.
 */
function plugins_get_plugin($module, $type, $id) {
  return PluginController::getInstance($module, $type)
    ->getPluginMetadata($id);
}

/**
 * Fetch a group of plugins by name.
 *
 * @param $module
 *   The name of the module that implements this plugin type.
 * @param $type
 *   The type identifier of the plugin.
 *
 * @return
 *   An array of information arrays about the plugins received. The contents
 *   of the array are specific to the plugin.
 */
function plugins_get_plugins($module, $type) {
  return PluginController::getInstance($module, $type)
    ->getAllPluginMetadata();
}

/**
 * Reset all static caches that affect plugin retrieval.
 */
function plugins_reset() {
  /*
  drupal_static_reset('plugins_type_all_info');
  // TODO This has moved.
  drupal_static_reset('_plugins_find_includes');
  drupal_static_reset('plugins_get_all_plugins');
  drupal_static_reset('plugins_get_all_plugins_fully_loaded');
  */
}
