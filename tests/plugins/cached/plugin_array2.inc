<?php
/**
 * @file
 * Plugin include using a plugin array to declare a plugin.
 */

/**
 * Plugin array plugin definition.
 */
$plugin = array(
  'function' => 'plugins_plugin_test_plugin_array2_cached_test',
  'class' => array(
    'class' => 'pluginsCachedPluginArray2',
  ),
);

/**
 * Plugin array function plugin.
 */
function plugins_plugin_test_plugin_array2_cached_test() {}
