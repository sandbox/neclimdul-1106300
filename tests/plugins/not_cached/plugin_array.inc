<?php
/**
 * @file
 * Plugin include using a plugin array to declare a plugin.
 */

/**
 * Plugin array plugin definition.
 */
$plugin = array(
  'function' => 'plugins_plugin_test_plugin_array_not_cached_test',
  'class' => array(
    'class' => 'pluginsNotCachedPluginArray',
  ),
);

/**
 * Plugin array function plugin.
 */
function plugins_plugin_test_plugin_array_not_cached_test() {}
