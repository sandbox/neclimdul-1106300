<?php

/**
 * Test Early plugin discovery features.
 *
 * By using a unit test we ensure that we don't have access to the database or
 * functions that require the database. This emulates early bootstaping where
 * these features won't be available.
 */
class PluginsEarlyDiscovery extends DrupalUnitTestCase {
  public static function getInfo() {
    return array(
      'name' => t('Plugin early discovery'),
      'description' => 'Verify that plugin definitions work in early bootstrap.',
      'group' => t('Plugins'),
    );
  }

  protected function assertPluginFunction($module, $type, $id, $function = 'function') {
    $func = plugins_load_function($module, $type, $id, $function);
    $this->assertTrue(function_exists($func), t('Plugin @plugin of plugin type @module:@type successfully retrieved @retrieved for @function.', array(
      '@plugin' => $id,
      '@module' => $module,
      '@type' => $type,
      '@function' => $function,
      '@retrieved' => $func,
    )));
  }

  protected function assertPluginMissingFunction($module, $type, $id, $function = 'function') {
    $func = plugins_load_function($module, $type, $id, $function);
    $this->assertEqual($func, NULL, t('Plugin @plugin of plugin type @module:@type for @function with missing function successfully failed.', array(
      '@plugin' => $id,
      '@module' => $module,
      '@type' => $type,
      '@function' => $func,
    )));
  }

  protected function assertPluginClass($module, $type, $id, $class = 'class') {
    $class_name = plugins_load_class($module, $type, $id, $class);
    $this->assertTrue(class_exists($class_name), t('Plugin @plugin of plugin type @module:@type successfully retrieved @retrieved for @class.', array(
      '@plugin' => $id,
      '@module' => $module,
      '@type' => $type,
      '@class' => $class,
      '@retrieved' => $class_name,
    )));
  }

  protected function assertPluginMissingClass($module, $type, $id, $class = 'class') {
    $class_name = plugins_load_class($module, $type, $id, $class);
    $this->assertEqual($class_name, NULL, t('Plugin @plugin of plugin type @module:@type for @class with missing class successfully failed.', array(
      '@plugin' => $id,
      '@module' => $module,
      '@type' => $type,
      '@class' => $class,
    )));
  }

  function testPluginDiscovery() {
    // TODO test that core stuff works...
  }
}

/**
 * Test basic plugins functionality.
 */
class PluginsGetInfoTestCase extends DrupalWebTestCase {
  public static function getInfo() {
    return array(
      'name' => t('Plugins unit tests'),
      'description' => 'Verify that plugin type definitions can properly set and overide definition values.',
      'group' => t('Plugins'),
    );
  }

  /**
   * Set up the test.
   */
  function setUp() {
    // Call parent setup with required modules.
    parent::setUp('plugins', 'plugins_test');
  }


  protected function assertPluginFunction($module, $type, $id, $function = 'function') {
    $func = plugins_load_function($module, $type, $id, $function);
    $this->assertTrue(function_exists($func), t('Plugin @plugin of plugin type @module:@type successfully retrieved @retrieved for @function.', array(
      '@plugin' => $id,
      '@module' => $module,
      '@type' => $type,
      '@function' => $function,
      '@retrieved' => $func,
    )));
  }

  protected function assertPluginMissingFunction($module, $type, $id, $function = 'function') {
    $func = plugins_load_function($module, $type, $id, $function);
    $this->assertEqual($func, NULL, t('Plugin @plugin of plugin type @module:@type for @function with missing function successfully failed.', array(
      '@plugin' => $id,
      '@module' => $module,
      '@type' => $type,
      '@function' => $func,
    )));
  }

  protected function assertPluginClass($module, $type, $id, $class = 'class') {
    $class_name = plugins_load_class($module, $type, $id, $class);
    $this->assertTrue(class_exists($class_name), t('Plugin @plugin of plugin type @module:@type successfully retrieved @retrieved for @class.', array(
      '@plugin' => $id,
      '@module' => $module,
      '@type' => $type,
      '@class' => $class,
      '@retrieved' => $class_name,
    )));
  }

  protected function assertPluginMissingClass($module, $type, $id, $class = 'class') {
    $class_name = plugins_load_class($module, $type, $id, $class);
    $this->assertEqual($class_name, NULL, t('Plugin @plugin of plugin type @module:@type for @class with missing class successfully failed.', array(
      '@plugin' => $id,
      '@module' => $module,
      '@type' => $type,
      '@class' => $class,
    )));
  }

  /**
   * Test that plugin definitions are loaded correctly.
   */
  public function testPluginLoading() {
    $module = 'plugins_test';
    $type = 'not_cached';

    // Test function retrieval for plugins using different definition methods.
    $this->assertPluginFunction($module, $type, 'plugin_array', 'function');
    $this->assertPluginFunction($module, $type, 'plugin_array2', 'function');
    $this->assertPluginMissingFunction($module, $type, 'plugin_array_dne', 'function');
    $this->assertPluginFunction($module, "big_hook_$type", 'test1', 'function');
    $this->assertPluginFunction($module, "${type}_info", 'not_cached', 'function');

    // Test class retrieval for plugins using different definition methods.
    $this->assertPluginClass($module, $type, 'plugin_array', 'class');
    $this->assertPluginClass($module, $type, 'plugin_array2', 'class');
    $this->assertPluginMissingClass($module, $type, 'plugin_array_dne', 'class');
    // TODO Test big hook plugins.

    $type = 'cached';

    // Test function retrieval for plugins using different definition methods.
    $this->assertPluginFunction($module, $type, 'plugin_array', 'function');
    $this->assertPluginFunction($module, $type, 'plugin_array2', 'function');
    $this->assertPluginMissingFunction($module, $type, 'plugin_array_dne', 'function');
    $this->assertPluginFunction($module, "big_hook_$type", 'test1', 'function');
    $this->assertPluginFunction($module, "${type}_info", 'cached', 'function');

    // Test class retrieval for plugins using different definition methods.
    $this->assertPluginClass($module, $type, 'plugin_array', 'class');
    $this->assertPluginClass($module, $type, 'plugin_array2', 'class');
    $this->assertPluginMissingClass($module, $type, 'plugin_array_dne', 'class');
    // TODO Test big hook plugins.

  }

  /**
   * Test that plugin array include hard caching works.
   */
  public function testPluginIncludeStaticCache() {
    $module = 'plugins_test';
    $type = 'not_cached';

    // Test function retrieval for plugins using different definition methods.
    $this->assertPluginFunction($module, $type, 'plugin_array', 'function');
  }
}
